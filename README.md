# A Graphical User Interface for Bayesian Microbial Growth Modeling

## Abstract

Traditional sigmoidal microbial growth models have a difficult time correctly modeling growth under non-standard conditions, such as alterations in the genetic code or similar experimental perturbations. These difficulties arise due to the non-sigmoidal growth curves which result from these changes. Additionally, traditional sigmoidal models also cannot account for batch and replicate effects. Previous research in the Schmid lab has produced a hierarchical Gaussian process model (referred to as Phenom) which has increased modeling potential for microbial growth under all conditions. This model takes into  account the uncertainty due to batch and replicate effects and can capture non-standard growth patterns. In this repository, we present a graphical user interface for the Phenom model. This GUI provides this microbial growth model to a wider range of researchers, facilitating novel research in microbiology for those who do not have extensive training in microbial computational tools.

**Keywords**: Microbial Growth, Gaussian Process Regression, GUI

## Datasets

Microbial growth data used in this project is provided by the [Schmid Lab](https://sites.biology.duke.edu/schmidlab/) at Duke University.

## Software and analytical approaches

The GUI runs the Phenom model created by the Schmid Lab which is a hierarchical Gaussian Process Regression (GPR) model.

## File description

* **FinalPaper.pdf**: The paper describing the project.
* **GUI_phenom**: The directory containing the files for the GUI
* **Installation_Guide**: A guide for installing the GUI to your local machine.

## GUI Usage and Installation
GUI usage and installation instructions are provided in the final paper. 

# Project Members

**Andrew Patterson, Chayut Wongkamthong, Joseph Krinke**, Duke University 

**Advisors**: 

Amy K. Schmid, Biology, Duke University

Scott C. Schmidler, Statistical Science, Duke University

Peter D Tonner, NRC Post-doc, NIST

Rylee Hackley, Ph.D. Candidate, Duke University
