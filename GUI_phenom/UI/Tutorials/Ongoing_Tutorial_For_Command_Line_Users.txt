﻿#Note: this tutorial is an ongoing draft, and is written towards those with less command line/ python experience, but with a grasp of the basics.


This is a tutorial for the Bayesian Hierarchical Gaussian process model GUI for processing microbial growth data. This tutorial is for users who are familiar with how to use Jupyter notebooks and simple command line interfaces, and wish to use the GUI on their local machine. A version of the GUI hosted on a cloud server is currently under development.


System Requirements:


OS requirements:
This software has been run on macOS, and Windows 10 specifically:
        Windows 10 version 10.0.18362 Build 18362
* osX 10.14.1 [need to confirm with team]
  * note, special steps may be needed to install *pystan* on osX
    operating systems. [See the pystan wiki for more details.](https://github.com/stan-dev/pystan/wiki/PyStan-and-OS-X)
        
Python and jupyter notebooks:


python and jupyter lab can be downloaded through anaconda (discuss if there are more efficient methods to do this). Download and install anaconda, which comes with an updated version of python. To install jupyter lab, open a conda terminal and run this command:


conda install -c conda-forge jupyterlab


There are several major dependencies which should be installed through python or conda
        In python, these can be installed with the command:
                
pip install *package name* 
        
        Alternatively, conda command line can install packages:
                
conda install *package name*
        
Packages to install:
 -pystan
 -numpy
 -patsy
 -matplotlib
 -GPy (for running examples)
 -pandas
 -Ipyvuetify
 -voila (confirm this one)


(confirm with teammates other packages as project evolves)




Installation:
        Open the command line and clone into github using the command:
                
git clone *clone ssh key*


        The clone ssh key is on github website for the GUI (will have more information here when finalized).
        
        Once cloned, navigate into the folder containing the file setup.py (this will change over time, will finalize path once the github is more complete). Next, in the command line run this command, which will install dependencies required by the program. This should
take under five minutes.


        python setup.py install


Running the GUI:
        
        The simplest way to run the GUI is through the command line interface. Navigate to the folder containing the file UI_phenom.ipynb and run this command:
        
python -m notebook UI_phenom.ipynb


This will start a jupyter notebook. In the top bar, select “Cell” and click “Run All.” Scroll to the bottom of the page, which will contain the GUI.


(discuss with teammates using voila instead, an alternative way to run the GUI)


Using the GUI:


(this is intentionally left blank at this point, because the GUI is currently undergoing overhauls and anything written will be obsolete. This is a placeholder)